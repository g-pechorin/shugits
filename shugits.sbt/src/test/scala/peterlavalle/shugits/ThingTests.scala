package peterlavalle.shugits

import java.io.File
import org.json.{JSONArray, JSONObject}

class ThingTests extends munit.FunSuite {

	test("parse that json") {
		val data =
			new JSONObject()
				.put("a", 9)
				.put("b", "queue")

		case class Kat(a: Int, b: String)

		val read: JSON[Kat] =
			import JSON._
			for {
				//a <- get[Int]
				//b <- get[String]
				a <- ("a")[Int]
				b <- "b"[String]
			} yield {
				Kat(a, b)
			}

		assertEquals(
			read(data),
			Left(Kat(9, "queue"))
		)


		val obtained = 42
		val expected = 42
		assertEquals(obtained, expected)
	}

	val cwd = new File("nope").ParentFile

	test("find oldest") {

		val Some(one) =
			Node.all(cwd)
				.find(_.parents.exists(_.replace("0", "").isEmpty))

		assertEquals(
			"69d0e01ddb351eec7d0a9a9db3505b341cbce760",
			one.node
		)
	}
	test("get oldest") {

		val List(one) =
			Node.rev(cwd, "0")

		assertEquals(
			"69d0e01ddb351eec7d0a9a9db3505b341cbce760",
			one.node
		)
	}
}
