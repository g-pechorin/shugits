package peterlavalle.shugits

import java.io.File

object Main:
	case class Config
	(
		cwd: File = new File("cwd").ParentFile,
		full: Boolean = false,
		forks: Boolean = true,
		mode: String = null,
	)

	def main(args: Array[String]): Unit =
		import scopt.OParser
		val builder = OParser.builder[Config]
		val parser = {
			import builder._
			OParser.sequence(

				programName("shugits"),
				head("scala hg git sync", "2024-04-05"),

				opt[File]('w', "cwd")
					.action((w, c) => c.copy(cwd = w))
					.text("working directory to find the .hg repo in"),

				//opt[Boolean]('f', "full")
				//	.action((f, c) => c.copy(full = f))
				//	.text("label all revisions - all of them"),

				cmd("bookmark")
					.action((_, c) => c.copy(mode = "bookmark"))
					.text("apply bookmarks to all heads")
			)
		}

		// now parse?
		OParser.parse(parser, args, Config())
			.foreach {
				config =>


					config.mode match
						case "bookmark" | null =>
							bookmark(config)
			}

	def bookmark(config: Config): Unit =

		// remove all bookmarks
		Node.all(config.cwd).map(_.bookmarks).reduce(_ ++ _)
			.foreach {
				bookmark =>
					import sys.process.*
					require(0 == {
						Process(
							Seq("hg", "bookmark", "-d", bookmark),
							config.cwd
						).!
					})
			}

		// check that all bookmarks are clear
		require(
			Node.all(config.cwd).forall(_.bookmarks.isEmpty)
		)

		// compute all leafs
		val leafs = {
			def loop(seen: Set[String], todo: List[ChangeSet]): LazyList[ChangeSet] =
				todo match
					case head :: tail =>
						if (seen(head.hash))
							loop(seen, tail)
						else if (head.children.isEmpty)
							head #:: loop(seen + head.hash, tail)
						else
							loop(seen + head.hash, head.children.foldRight(tail)(_ :: _))
					case Nil =>
						LazyList()

			loop(
				Set(),
				List(ChangeSet(config.cwd))
			)
		}

		// apply NEW labels
		leafs.foreach {
			leaf =>
				leaf.bookmark {
					"hg." + leaf.branch
				}
		}
