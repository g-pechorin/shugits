package peterlavalle.shugits

import java.io.File

trait CLI[T] {
	def execute(cwd: File, args: Array[String]): T
}

object CLI:

	def main(m:CLI[_])=

		val cwd = new File("cwd").ParentFile
		
	
	trait ArgType[T]:
		def named(key: String): T

	trait Block[T]:
		def map[O](f: T => O): CLI[O]

	sealed trait Typer:
		def block[T](o: Maybe[T], f: ArgType[T]): Block[T]

		final def apply[T: ArgType]: Block[T] =
			block[T](Maybe(), summon[ArgType[T]])

		final def apply[T: ArgType](t: => T): Block[T] =
			block[T](Maybe(t), summon[ArgType[T]])


	given ArgType[String] =
		(key: String) => stub.toString

	def req(key: String, alt: String*): Typer =
		new Typer:
			override def block[T](m: Maybe[T], f: ArgType[T]): Block[T] =
				new Block[T]:
					override def map[O](f: T => O): CLI[O] =
						(cwd: File, args: Array[String]) =>
							stub

