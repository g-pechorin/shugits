package peterlavalle

import java.io.File

package object shugits:
	extension (file: File)
		def AbsoluteFile: File =
			val absoluteFile = file.getAbsoluteFile
			if (absoluteFile == file)
				file
			else
				absoluteFile
		def AbsolutePath: String = AbsoluteFile.getAbsolutePath.replace('\\', '/')
		def ParentFile: File = AbsoluteFile.getParentFile
		def /(path: String): File = File(AbsoluteFile, path).AbsoluteFile

	def stub: Nothing =
		val exception = new NotImplementedError()
		exception.setStackTrace(exception.getStackTrace.tail)
		throw exception