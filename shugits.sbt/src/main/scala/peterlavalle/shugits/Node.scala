package peterlavalle.shugits

import java.io.File

case class Node
(
	node: String
)(
	val branch: String,
	val parents: Set[String],
	val bookmarks: Set[String],
)

object Node:
	given json: JSON[Node] =
		import JSON._
		for {
			node <- "node"[String]
			branch <- "branch"[String]
			parents <- "parents".array[String]
			bookmarks <- "bookmarks".array[String]
		} yield {
			Node(
				node,
			)(
				branch,
				parents.toSet,
				bookmarks.toSet
			)
		}

	def all(hg: File): List[Node] =
		import sys.process.*
		import org.json.{JSONArray, JSONObject}
		val cwd = {
			var cwd = hg
			while (!(cwd / ".hg").isDirectory)
				cwd = cwd.ParentFile
			cwd
		}

		val text: String =
			Process(
				Seq("hg", "log", "-Tjson"),
				cwd
			).!!
		Node.json(new JSONArray(text)) match
			case Left(list) =>
				list

	def rev(hg: File, rev: String): List[Node] =
		import sys.process.*
		import org.json.{JSONArray, JSONObject}
		val cwd = {
			var cwd = hg
			while (!(cwd / ".hg").isDirectory)
				cwd = cwd.ParentFile
			cwd
		}

		val text: String =
			Process(
				Seq("hg", "log", "-r", rev, "-Tjson"),
				cwd
			).!!
		Node.json(new JSONArray(text)) match
			case Left(list) =>
				list

