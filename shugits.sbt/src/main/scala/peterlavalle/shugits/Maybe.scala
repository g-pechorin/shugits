package peterlavalle.shugits

trait Maybe[T]:
	def value: T

	def orElse(v: => T): T

	def isEmpty: Boolean

object Maybe:
	private val none =
		new Maybe[Nothing]:
			override def isEmpty: Boolean = true

			override def orElse(v: => Nothing): Nothing = v

			override def value: Nothing =
				sys.error("can't open an empty")


	def apply[T](): Maybe[T] =
		none.asInstanceOf[Maybe[T]]

	def apply[T](t: => T): Maybe[T] =
		new Maybe[T]:
			override def orElse(v: => T): T = value

			override def isEmpty: Boolean = false

			override lazy val value: T = t
