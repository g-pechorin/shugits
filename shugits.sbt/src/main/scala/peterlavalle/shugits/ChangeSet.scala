package peterlavalle.shugits

import java.io.File

class ChangeSet(hg: File, rev: String = "0") {
	val node: Node =
		Node.rev(hg, rev) match
			case List(node) =>
				node
	val hash: String = node.node
	val branch: String = node.branch
	lazy val parents: Set[ChangeSet] = node.parents.map(ChangeSet(hg, _))

	lazy val children: Set[ChangeSet] =
		Node.all(hg).filter(_.parents.contains(hash))
			.map(_.node)
			.toSet
			.map(ChangeSet(hg, _))

	def bookmark(name: String): Unit =
		import sys.process.*
		require(0 == {
			Process(
				Seq("hg", "bookmark", "-f", "-r", hash, name),
				hg
			).!
		})
}


